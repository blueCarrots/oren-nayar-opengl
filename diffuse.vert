#version 400

in vec4 vp;
uniform mat4 u_modelViewPerspective;

void main () {
  gl_Position = u_modelViewPerspective * vp;
  /*gl_Position = mat4(1) * vec4 (vp, 1.0);*/
};
