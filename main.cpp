#include <GL/glew.h>
#include <GLFW/glfw3.h> // GLFW helper library
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/matrix_transform.hpp>
#define TAU (M_PI * 2.0)

char* read_from_buffer(const char* filename);
void printLinkInfoLog(int prog);
void printShaderInfoLog(int shader);

void printLinkInfoLog(int prog)
{
  int infoLogLen = 0;
  int charsWritten = 0;
  GLchar *infoLog;
  
  glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &infoLogLen);

  // should additionally check for OpenGL errors here
  
  if (infoLogLen > 0)
  {
    infoLog = new GLchar[infoLogLen];
    // error check for fail to allocate memory omitted
    glGetProgramInfoLog(prog,infoLogLen, &charsWritten, infoLog);
    std::cout << "InfoLog:" << std::endl << infoLog << std::endl;
    delete [] infoLog;
  }
} 
  
void printShaderInfoLog(int shader)
{
  int infoLogLen = 0;
  int charsWritten = 0;
  GLchar *infoLog;

  glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLen);

  // should additionally check for OpenGL errors here

  if (infoLogLen > 0)
  {
    infoLog = new GLchar[infoLogLen];
    // error check for fail to allocate memory omitted
    glGetShaderInfoLog(shader,infoLogLen, &charsWritten, infoLog);
    std::cout << "InfoLog:" << std::endl << infoLog << std::endl;
    delete [] infoLog;
  }

  // should additionally check for OpenGL errors here
}

//From http://tinyurl.com/7pqounf 
//Returns heap-allocated memory that needs to be freed later.
char* read_from_buffer(const char* filename){
  std::ifstream t;
  long length;
  //TODO: There really should be a try... catch here.
  t.open(filename);      // open input file
  t.seekg(0, std::ios::end);    // go to the end
  length = t.tellg();           // report location (this is the length)
  t.seekg(0, std::ios::beg);    // go back to the beginning
  char* buffer = new char[length];    // allocate memory for a buffer of appropriate dimension
  t.read(buffer, length);       // read the whole file into the buffer
  t.close();                    // close file handle 
  return buffer;
}

//Code from: http://antongerdelan.net/opengl/hellotriangle.html
int main () {
  // start GL context and O/S window using the GLFW helper library
  if (!glfwInit ()) {
    fprintf (stderr, "ERROR: could not start GLFW3\n");
    return 1;
  } 

	// uncomment these lines if on Apple OS X
  /*glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 2);
  glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);*/

  GLFWwindow* window = glfwCreateWindow (640, 480, "Hello Triangle", NULL, NULL);
  if (!window) {
    fprintf (stderr, "ERROR: could not open window with GLFW3\n");
    glfwTerminate();
    return 1;
  }
  glfwMakeContextCurrent (window);
                                  
  // start GLEW extension handler
  glewExperimental = GL_TRUE;
  glewInit ();

  // get version info
  const GLubyte* renderer = glGetString (GL_RENDERER); // get renderer string
  const GLubyte* version = glGetString (GL_VERSION); // version as a string
  printf ("Renderer: %s\n", renderer);
  printf ("OpenGL version supported %s\n", version);

  // tell GL to only draw onto a pixel if the shape is closer to the viewer
  glEnable (GL_DEPTH_TEST); // enable depth-testing
  glDepthFunc (GL_LESS); // depth-testing interprets a smaller value as "closer"

  /* OTHER STUFF GOES HERE NEXT */

  //column major order (first row is first vertex, second row is second
  //vertex, etc).
  float points[] = {
   -0.5f,  0.5f,  0.0f, 1.0,
   0.5f, -0.5f,  0.0f, 1.0,
  -0.5f, -0.5f,  0.0f, 1.0
  }; 

  GLuint vbo = 0;
  glGenBuffers (1, &vbo);
  glBindBuffer (GL_ARRAY_BUFFER, vbo);
  glBufferData (GL_ARRAY_BUFFER, 12 * sizeof (float), points, GL_STATIC_DRAW);
  
  GLuint vao = 0;
  glGenVertexArrays (1, &vao);
  glBindVertexArray (vao);
  glEnableVertexAttribArray (0);
  glBindBuffer (GL_ARRAY_BUFFER, vbo);
  //I think the below teslls us what kind of buffer "vbo" (at location 0) is...
  glVertexAttribPointer (0, 4, GL_FLOAT, GL_FALSE, 0, NULL);
  
  char* vertex_shader = read_from_buffer("diffuse.vert");
  char* fragment_shader = read_from_buffer("diffuse_lambert.frag");

  GLuint vs = glCreateShader (GL_VERTEX_SHADER);
  glShaderSource (vs, 1, &vertex_shader, 0);
  glCompileShader (vs);
  GLuint fs = glCreateShader (GL_FRAGMENT_SHADER);
  glShaderSource (fs, 1, &fragment_shader, 0);
  glCompileShader (fs);

  GLuint shader_programme = glCreateProgram ();
  glAttachShader (shader_programme, fs);
  glAttachShader (shader_programme, vs);
  glLinkProgram (shader_programme);
  
  GLint compiled;
  glGetShaderiv(vs, GL_COMPILE_STATUS, &compiled);
  if (!compiled)
    printShaderInfoLog(vs);
  glGetShaderiv(fs, GL_COMPILE_STATUS, &compiled);
  if (!compiled)
    printShaderInfoLog(fs);

  GLint linked;
  glGetProgramiv(shader_programme,GL_LINK_STATUS, &linked);
  if (!linked)
    printLinkInfoLog(shader_programme);

  glUseProgram(shader_programme);


  GLint mvp_uniform_location = 
    glGetUniformLocation(shader_programme, "u_modelViewPerspective");
  std::cout << "mvp uniform location: " << mvp_uniform_location << std::endl;

  GLenum err = glGetError();
  if( err ) 
    printf("Get uniform location Error: %x\n", err);  
  
  //glm::mat4 projection = glm::ortho(-1.0f, 1.0f, 1.0f, -1.0f);
  glm::mat4 projection = glm::mat4(1); 
  glm::mat4 view = glm::mat4(1);
  glm::mat4 model = glm::mat4(1);
  float theta = 0.0f;
  err = glGetError();
  if( err ) 
    printf("Set uniform Error: %x\n", err);  

  double currentFrame = glfwGetTime();
  double lastFrame = currentFrame;
  double deltaTime;
  double speed = 0.6;

  glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
  glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glViewport(512, 512, 512, 512);
  glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

  while (!glfwWindowShouldClose (window)) {
    currentFrame = glfwGetTime();
    deltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;
    theta = theta + deltaTime * speed;

    if( theta > TAU )
      theta -= TAU;

    model[0][0] = cos(theta); 
    model[0][2] = sin(theta);
    model[2][0] = -sin(theta); 
    model[2][2] = cos(theta); 

    /* Below is Z-axis rotation
     *
     *model[0][0] = cos(theta); 
     *model[0][1] = -sin(theta); 
     *model[1][0] = sin(theta); 
     *model[1][1] = cos(theta); 
     */

    glm::mat4 pvm = projection * model * view;
    //set uniform
    glUniformMatrix4fv(mvp_uniform_location, 1, GL_FALSE, &pvm[0][0]);

    // wipe the drawing surface clear
    glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram (shader_programme);
    glBindVertexArray (vao);
    // draw points 0-3 from the currently bound VAO with current in-use shader
    glDrawArrays (GL_TRIANGLES, 0, 3);
    // update other events like input handling 
    glfwPollEvents ();
    // put the stuff we've been drawing onto the display
    glfwSwapBuffers (window);
  }

  // close GL context and any other GLFW resources
  glfwTerminate();
  return 0;
}
